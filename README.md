# Brick Braker Game

The project is very suggestive by name. This is a game where, with the help of a ball, the bricks will 
be broken which are above from a line, a line that keeps the ball in game.

# Setup

In this project, I have used several generic classes, ->"JFrame" which seemed to me the most spectacular,
because it creates the framework in which the game will run, this magic comes from nothing, JFrame crate 
a world from scratch, after that, it comes the "Jpanel", which provide us some buttons.

Also in the project I have used -> KeyListener, a interface where I can put some KeyEvents, left and right
								-> ActionListener, a interface which has only one method, I used thie method
								to specify when the game should start.
								
	-> with the fillRect and setColor method, I gave values, dimensions and color, for the elements in the frame,
		they were in the Graphics class. 
	
	I imported different classes from swing and awt. I have instantiated several generic classes, I instantiated 
	the Logic class.
	
# Further Work

I want to insert sound when the bricks are broken and when the ball hit the paddle.
I want to build more levels of play and I want to add different textures for bricks.


package brickbracker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Logic extends JPanel implements KeyListener, ActionListener {

    private boolean play = false;
    private int score = 0;

    private int totalBricks = 21;

    private Timer time;

    private int delay = 8;
    private int playerX = 310;

    private int ballPosX = 120;
    private int ballPosY = 350;
    private int ballxdir = -1;
    private int ballydir = -2;

    private BricksGenerator bricks;

    public Logic(){
        bricks = new BricksGenerator(3, 7);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        time = new Timer(delay, this);
        time.start();
    }

    public void paint(Graphics graphics){
        //backgroung
        graphics.setColor(Color.black);
        graphics.fillRect(1,1,698,598);

        //drawing map
        bricks.draw((Graphics2D)graphics);

        //borders
        graphics.setColor(Color.yellow);
        graphics.fillRect(0,0,3,598);
        graphics.fillRect(0,0,698,3);
        graphics.fillRect(698,0,3,598);

        //scores
        graphics.setColor(Color.white);
        graphics.setFont(new Font("serif", Font.BOLD, 25));
        graphics.drawString(""+score, 590, 30);

        //the paddle
        graphics.setColor(Color.green);
        graphics.fillRect(playerX, 550, 100,8);

        //the ball
        graphics.setColor(Color.yellow);
        graphics.fillOval(ballPosX, ballPosY, 20,20);

        if(totalBricks == 0){
            play = false;
            ballxdir = 0;
            ballydir = 0;
            graphics.setColor(Color.RED);
            graphics.setFont(new Font("serif", Font.BOLD, 30));
            graphics.drawString("You won " + score, 260, 300);

            graphics.setFont(new Font("serif", Font.BOLD, 20));
            graphics.drawString("Press enter to restart !!", 220, 350);
        }

        if(ballPosY > 570){
            play = false;
            ballxdir = 0;
            ballydir = 0;
            graphics.setColor(Color.RED);
            graphics.setFont(new Font("serif", Font.BOLD, 30));
            graphics.drawString("Game Over, Scores: " + score, 190, 350);

            graphics.setFont(new Font("serif", Font.BOLD, 20));
            graphics.drawString("Press enter to restart !!", 180, 375);
        }

        graphics.dispose();

    }

    public void actionPerformed(ActionEvent e) {
        time.start();

        if(play){
            if(new Rectangle(ballPosX, ballPosY, 20, 20).intersects(
                    new Rectangle(playerX, 550, 100, 8))){
                    ballydir = -ballydir;
            }

            A: for(int i = 0; i < bricks.map.length; i++){
                for(int j = 0; j < bricks.map[0].length; j++){
                        if(bricks.map[i][j] > 0){
                            int brickX = j * bricks.brickWidth + 80;
                            int brickY = i * bricks.brickHeight + 50;
                            int brickWidth = bricks.brickWidth;
                            int brickHeight = bricks.brickHeight;

                            Rectangle rect = new Rectangle(brickX, brickY, brickWidth, brickHeight);
                            Rectangle ballRect = new Rectangle(ballPosX, ballPosY, 20, 20);
                            Rectangle brickRect = rect;

                            if(ballRect.intersects(brickRect)) {
                                bricks.setBrickValue(0, i, j);
                                totalBricks--;
                                score += 5;

                                if (ballPosX + 19 <= brickRect.x || ballPosX + 1 >= brickRect.x + brickRect.width) {
                                    ballxdir = -ballxdir;
                                } else {
                                    ballydir = -ballydir;
                                }
                                break A;
                            }
                        }
                }
            }


            ballPosX += ballxdir;
            ballPosY += ballydir;

            if(ballPosX < 0){
                ballxdir = -ballxdir;
            }
            if(ballPosY < 0){
                ballydir = -ballydir;
            }
            if(ballPosX > 678){
                ballxdir = -ballxdir;
            }
        }


        repaint();
    }

    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                if(playerX >= 598){
                    playerX = 598;
                }else{
                    moveRight();
                }
        }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
                if(playerX <= 13){
                    playerX = 13;
                }else{
                    moveLeft();
                }
        }

        if(e.getKeyCode() == KeyEvent.VK_ENTER){
            if(!play){
                play = true;
                ballPosX = 120;
                ballPosY = 350;
                ballxdir = -1;
                ballydir = -2;
                playerX = 310;
                score = 0;
                totalBricks = 21;
                bricks = new BricksGenerator(3, 7);

                repaint();
            }
        }
    }

    public void moveRight(){
        play = true;
        playerX+=20;
    }

    public void moveLeft(){
        play = true;
        playerX-=20;
    }

    public void keyTyped(KeyEvent e) {}

    public void keyReleased(KeyEvent e) {}
}
